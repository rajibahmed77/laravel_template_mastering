
<!DOCTYPE html>
<html lang="zxx">

<head>
    <title>Sunglass E-Shop | @yield ('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8">
    <meta name="keywords" content="Goggles a Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
    <script>
        addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <link href="{{asset('ui/frontend')}}/css/bootstrap.css" rel='stylesheet' type='text/css' />
    <link href="{{asset('ui/frontend')}}/css/login_overlay.css" rel='stylesheet' type='text/css' />
    <link href="{{asset('ui/frontend')}}/css/style6.css" rel='stylesheet' type='text/css' />
    <link rel="stylesheet" href="{{asset('ui/frontend')}}/css/shop.css" type="text/css" />
    <link rel="stylesheet" href="{{asset('ui/frontend')}}/css/owl.carousel.css" type="text/css" media="all">
    <link rel="stylesheet" href="{{asset('ui/frontend')}}/css/owl.theme.css" type="text/css" media="all">
    <link href="{{asset('ui/frontend')}}/css/style.css" rel='stylesheet' type='text/css' />
    <link href="{{asset('ui/frontend')}}/css/fontawesome-all.css" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Inconsolata:400,700" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800"
          rel="stylesheet">
</head>

<body>
<div class="banner-top container-fluid" id="home">


    <!-- header -->
@include('frontend.layouts.partial.header')



@yield ('index')
@yield('singleProductView')
@yield('Shop_Catagory')
@yield('aboutPage')
@yield('Contact_Us')
@yield('customer')
@yield('payment')
@yield('checkout')
@yield('404')


@include('frontend.layouts.partial.footer')





<!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center p-5 mx-auto mw-100">
                    <h6>Join our newsletter and get</h6>
                    <h3>50% Off for your first Pair of Eye wear</h3>
                    <div class="login newsletter">
                        <form action="#" method="post">
                            <div class="form-group">
                                <label class="mb-2">Email address</label>
                                <input type="email" class="form-control" id="exampleInputEmail2" aria-describedby="emailHelp" placeholder="" required="">
                            </div>
                            <button type="submit" class="btn btn-primary submit mb-4">Get 50% Off</button>
                        </form>
                        <p class="text-center">
                            <a href="#">No thanks I want to pay full Price</a>
                        </p>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $("#myModal").modal();
        });
    </script>
    <!-- // modal -->

































































    <!--jQuery-->
    <script src="{{asset('ui/frontend')}}/js/jquery-2.2.3.min.js"></script>

    <!-- FlexSlider -->
    <script src="{{asset('ui/frontend')}}/js/jquery.flexslider.js"></script>
    <script>
        // Can also be used with $(document).ready()
        $(window).load(function () {
            $('.flexslider1').flexslider({
                animation: "slide",
                controlNav: "thumbnails"
            });
        });
    </script>
    <!-- //FlexSlider-->






    <!--search jQuery-->
    <script src="{{asset('ui/frontend')}}/js/modernizr-2.6.2.min.js"></script>
    <script src="{{asset('ui/frontend')}}/js/classie-search.js"></script>
    <script src="{{asset('ui/frontend')}}/js/demo1-search.js"></script>
    <!--//search jQuery-->
    <!-- cart-js -->
    <script src="{{asset('ui/frontend')}}/js/minicart.js"></script>
    <script>
        googles.render();

        googles.cart.on('googles_checkout', function (evt) {
            var items, len, i;

            if (this.subtotal() > 0) {
                items = this.items();

                for (i = 0, len = items.length; i < len; i++) {}
            }
        });
    </script>
    <!-- //cart-js -->
    <script>
        $(document).ready(function () {
            $(".button-log a").click(function () {
                $(".overlay-login").fadeToggle(200);
                $(this).toggleClass('btn-open').toggleClass('btn-close');
            });
        });
        $('.overlay-close1').on('click', function () {
            $(".overlay-login").fadeToggle(200);
            $(".button-log a").toggleClass('btn-open').toggleClass('btn-close');
            open = false;
        });
    </script>
    <!-- carousel -->
    <!-- Count-down -->
    <script src="{{asset('ui/frontend')}}/js/simplyCountdown.js"></script>
    <link href="{{asset('ui/frontend')}}/css/simplyCountdown.css" rel='stylesheet' type='text/css' />
    <script>
        var d = new Date();
        simplyCountdown('simply-countdown-custom', {
            year: d.getFullYear(),
            month: d.getMonth() + 2,
            day: 25
        });
    </script>
    <!--// Count-down -->
    <script src="{{asset('ui/frontend')}}/js/owl.carousel.js"></script>
    <script>
        $(document).ready(function () {
            $('.owl-carousel').owlCarousel({
                loop: true,
                margin: 10,
                responsiveClass: true,
                responsive: {
                    0: {
                        items: 1,
                        nav: true
                    },
                    600: {
                        items: 2,
                        nav: false
                    },
                    900: {
                        items: 3,
                        nav: false
                    },
                    1000: {
                        items: 4,
                        nav: true,
                        loop: false,
                        margin: 20
                    }
                }
            })
        })
    </script>

    <!-- //end-smooth-scrolling -->


    <!-- dropdown nav -->
    <script>
        $(document).ready(function () {
            $(".dropdown").hover(
                function () {
                    $('.dropdown-menu', this).stop(true, true).slideDown("fast");
                    $(this).toggleClass('open');
                },
                function () {
                    $('.dropdown-menu', this).stop(true, true).slideUp("fast");
                    $(this).toggleClass('open');
                }
            );
        });
    </script>
    <!-- //dropdown nav -->
    <script src="{{asset('ui/frontend')}}/js/move-top.js"></script>
    <script src="{{asset('ui/frontend')}}/js/easing.js"></script>
    <script>
        jQuery(document).ready(function($) {
            $(".scroll").click(function(event) {
                event.preventDefault();
                $('html,body').animate({
                    scrollTop: $(this.hash).offset().top
                }, 900);
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            /*
                                    var defaults = {
                                          containerID: 'toTop', // fading element id
                                        containerHoverID: 'toTopHover', // fading element hover id
                                        scrollSpeed: 1200,
                                        easingType: 'linear'
                                     };
                                    */

            $().UItoTop({
                easingType: 'easeOutQuart'
            });

        });
    </script>
    <!--// end-smoth-scrolling -->

    <script src="{{asset('ui/frontend')}}/js/bootstrap.js"></script>



    <!-- js file -->
</body>

</html>