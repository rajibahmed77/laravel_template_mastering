<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/index.html', function () {
    return view('welcome');
});


Route::get('/single.html', function () {
    return view('frontend.layouts.singleProduct');
});


Route::get('/shop.html', function () {
    return view('frontend.layouts.shop');
});

Route::get('/about.html', function () {
    return view('frontend.layouts.about');
});


Route::get('/contact.html', function () {
    return view('frontend.layouts.contact');
});

Route::get('/customer.html', function () {
    return view('frontend.layouts.customer');
});

Route::get('/payment.html', function () {
    return view('frontend.layouts.payment');
});

Route::get('/checkout.html', function () {
    return view('frontend.layouts.checkout');
});

Route::get('/404.html', function () {
    return view('frontend.layouts.404');
});



//backend routes......................................................>>



Route::get('/login.html', function () {
    return view('backend.layouts.login');
});

Route::get('/blank.html', function () {
    return view('backend.layouts.blank');
});

Route::get('/buttons.html', function () {
    return view('backend.layouts.buttons');
});

Route::get('/flot.html', function () {
    return view('backend.layouts.flot');
});

Route::get('/forms.html', function () {
    return view('backend.layouts.forms');
});

Route::get('/grid.html', function () {
    return view('backend.layouts.grid');
});

Route::get('/icons.html', function () {
    return view('backend.layouts.icons');
});

Route::get('/admin_index.html', function () {
    return view('backend.layouts.admin_index');
});


Route::get('/morris.html', function () {
    return view('backend.layouts.morris');
});

Route::get('/notifications.html', function () {
    return view('backend.layouts.notification');
});

Route::get('/panels-wells.html', function () {
    return view('backend.layouts.panels-wells');
});

Route::get('/tables.html', function () {
    return view('backend.layouts.tables');
});

Route::get('/typography.html', function () {
    return view('backend.layouts.typography');
});